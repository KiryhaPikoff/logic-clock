package ru.ulstu.rvip.logicclock

import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentLinkedQueue

@Component
class Channel {

    private val channel = ConcurrentLinkedQueue<Any>()

    fun push(message: Any) {
        channel.add(message)
    }

    fun poll(): Any? {
        return channel.poll()
    }

}
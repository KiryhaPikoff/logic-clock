package ru.ulstu.rvip.logicclock

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
class LogicClockApplication

fun main(args: Array<String>) {
	runApplication<LogicClockApplication>(*args)
}

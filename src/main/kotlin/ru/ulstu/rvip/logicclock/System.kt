package ru.ulstu.rvip.logicclock

import org.slf4j.LoggerFactory.getLogger
import org.springframework.scheduling.annotation.Scheduled
import java.time.LocalDateTime.now
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.max

abstract class System(

    private val channel: Channel

) {

    private val log = getLogger(this::class.java)
    private val localTime = AtomicInteger(0)

    open fun pushExchange() {
        val currentTime = localTime.incrementAndGet()
        val exchange = mapOf(
            "message" to "Hello, our logical time is $currentTime, physical time is ${now()}",
            "time" to currentTime
        )
        log.info("Push exchange with logic localTime=$currentTime")
        channel.push(exchange)
    }

    open fun pollExchange() {
        channel.poll() ?. let {
            val exchange = it as Map<String, *>
            val exchangeTime = exchange["time"] as Int
            localTime.updateAndGet { currentTime ->
                val setUpTime = max(currentTime, exchangeTime) + 1
                log.info(
                    "Poll exchange," +
                            " localTime=$currentTime," +
                            " exchangeTime=$exchangeTime," +
                            " timeForSetUp=$setUpTime"
                )
                setUpTime
            }
        }
    }

    @Scheduled(cron = "*/1 * * * * *")
    open fun printCurrentTime() {
        log.warn("Current local time is: $localTime")
    }

}
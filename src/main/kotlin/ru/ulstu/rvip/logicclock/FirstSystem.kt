package ru.ulstu.rvip.logicclock

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class FirstSystem(

    channel: Channel

): System(channel) {

    @Scheduled(cron = "*/3 * * * * *")
    override fun pushExchange() {
        super.pushExchange()
    }

    @Scheduled(cron = "*/2 * * * * *")
    override fun pollExchange() {
        super.pollExchange()
    }
}
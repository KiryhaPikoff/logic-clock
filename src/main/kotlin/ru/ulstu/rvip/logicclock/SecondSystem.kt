package ru.ulstu.rvip.logicclock

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class SecondSystem(

    channel: Channel

): System(channel) {

    @Scheduled(cron = "*/2 * * * * *")
    override fun pushExchange() {
        super.pushExchange()
    }

    @Scheduled(cron = "*/3 * * * * *")
    override fun pollExchange() {
        super.pollExchange()
    }
}